package com.hw.db.controllers;

import java.sql.Timestamp;
import java.util.Collections;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import com.hw.db.models.Thread;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class threadControllerTests {
    private Thread stubThread;

    @BeforeEach
    @DisplayName("Thread creation for test")
    void createThreadTest() {
        stubThread = new Thread(
                "Madina",
                new Timestamp(456787654678L),
                "forum",
                "kek",
                "slug",
                "Someday I'll be a hokage",
                19
        );

    }

    @Test
    @DisplayName("Create thread")
    void correctlyCreatesThreadTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(stubThread);
            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(
                            Collections.emptyList()
                    ),
                    controller.createPost("slug", Collections.emptyList()),
                    "Creating posts"
            );
            assertEquals(stubThread, ThreadDAO.getThreadBySlug("slug"));
        }
    }

    @Test
    @DisplayName("Get posts")
    void getPostsTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {

            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(stubThread);

            threadMock.when(() -> ThreadDAO.getPosts(stubThread.getId(), 10, 3, "no", false))
                    .thenReturn(Collections.emptyList());
            assertEquals(
                    ResponseEntity
                            .status(HttpStatus.OK)
                            .body(Collections.emptyList()),
                    controller.Posts("slug", 10, 3, "no", false),
                    "Post slug"
            );
        }
    }

    @Test
    @DisplayName("Get info of thread")
    void getThreadInfoTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(stubThread);
            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity
                            .status(HttpStatus.CREATED)
                            .body(Collections.emptyList()),
                    controller.createPost("slug", Collections.emptyList()),
                    "Creating posts"
            );
            assertEquals(
                    ResponseEntity
                            .status(HttpStatus.OK)
                            .body(stubThread),
                    controller.info("slug"),
                    "Get info"
            );
        }
    }

    @Test
    @DisplayName("Delete thread")
    void deleteThreadTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(stubThread);
            threadController controller = new threadController();
            stubThread.setId(2);
            assertEquals(
                    ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(Collections.emptyList()),
                    controller.createPost("slug", Collections.emptyList()),
                    "Creating posts"
            );
            assertEquals(
                    ResponseEntity
                    .status(HttpStatus.OK)
                    .body(null),
                    controller.change("slug", null),
                    "Delete thread"
            );
        }
    }

    @Test
    @DisplayName("Change thread")
    void changeThreadTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            String anotherSlug = "anotherSlug";
            Thread threadToBeChanged = new Thread(
                    "Hokage",
                    new Timestamp(12323435),
                    "forum",
                    "I am Hokage",
                    anotherSlug,
                    "I want it ramen",
                    32
            );
            threadToBeChanged.setId(2);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(anotherSlug)).thenReturn(threadToBeChanged);
            threadMock.when(() -> ThreadDAO.getThreadById(2)).thenReturn(stubThread);
            assertEquals(
                    ResponseEntity
                    .status(HttpStatus.OK)
                    .body(stubThread),
                    controller.change(anotherSlug, stubThread),
                    "Thread was changed"
            );
        }
    }


    @Test
    @DisplayName("Create vote")
    void createVoteTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(stubThread);
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User stubUser = new User(
                        "hokage", "hokage@konoha.ju", "Pumpkin", "No info"
                );
                userMock.when(() -> UserDAO.Info("hokage")).thenReturn(stubUser);
                threadController controller = new threadController();
                assertEquals(
                        ResponseEntity
                        .status(HttpStatus.OK)
                        .body(stubThread),
                        controller.createVote("slug", new Vote(stubUser.getNickname(), 32)),
                        "Passing class vote null object"
                );
            }
        }
    }
}

